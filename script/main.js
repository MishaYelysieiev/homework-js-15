const startBtn = document.getElementById("start");
const clearBtn = document.getElementById("clear");
const pauseBtn = document.createElement("button");
const wrap = document.getElementsByClassName("wrapper")[0];
let pos = "translate(-50%,-100%)"

pauseBtn.innerText = "PAUSE";
pauseBtn.classList.add("btn");

const minArrow = document.getElementById("min");
const secArrow = document.getElementById("sec");
const millArrow = document.getElementById("mil");

let minVal = 1;
let secVal = 1;
let millVal = 1;

startBtn.addEventListener("click",()=>{
    startBtn.remove();
    wrap.insertBefore(pauseBtn,wrap.firstChild);
    let minRun = setInterval(()=>{
        minArrow.style.transform = `${pos} rotateZ(${minVal*0.1}deg)`;
        minVal++;
    },1000);
    let secRun = setInterval(()=>{
        secArrow.style.transform = `${pos} rotateZ(${secVal*0.06}deg)`;
        secVal++;
    },10);
    let millRun = setInterval(()=>{
        millArrow.style.transform = `${pos} rotateZ(${millVal*3.6}deg)`;
        millVal++;
    },10);
    
    clearInt =()=>{
        clearInterval(minRun);
        clearInterval(secRun);
        clearInterval(millRun);
        pauseBtn.remove();
        wrap.insertBefore(startBtn,wrap.firstChild)  
    };
    pauseBtn.addEventListener("click",()=>{
        clearInt();
    });
    clearBtn.addEventListener("click",()=>{
        minArrow.style.transform = `${pos} rotateZ(0deg)`;
        secArrow.style.transform = `${pos} rotateZ(0deg)`;
        millArrow.style.transform = `${pos} rotateZ(0deg)`;
        clearInt();
        minVal=1;
        secVal=1;
        millVal=1;
    });
});
